from common_module import Common
import aiohttp
from aiohttp import web
import asyncio
import threading


class DictionaryCheckServer(Common.CheckingServer):
    @staticmethod
    async def make_all_requests_and_send_on_finish(queue_sender: Common.QueueSender, session: aiohttp.ClientSession,
                                                   url: str):
        response = await DictionaryCheckServer.make_all_http_requests(session, url)
        response = {'url': response['url'],
                    'methods_available': [x['method'] for x in response['responses']
                                          if x['response'].status < 400 or x['response'].status >= 500]}
        if response['methods_available']:
            queue_sender.send_to_queue({'got': response})
        else:
            print(f'[NO METHODS AVAILABLE] FOR {url}')

    @staticmethod
    async def checking_thread(queue_sender: Common.QueueSender, service_url: str):
        tasks = []
        async with aiohttp.ClientSession() as session:
            for url in DictionaryCheckServer.read_dictionary('test.txt'):
                tasks.append(DictionaryCheckServer.make_all_requests_and_send_on_finish(queue_sender,
                                                                                        session,
                                                                                        service_url + f'/{url}'))

            await asyncio.gather(*tasks, return_exceptions=True)

        queue_sender.send_to_queue({'status': 'finished', 'service': 'dict'})

    async def check(self, request: aiohttp.web.Request):
        data = await request.json()
        service_url = data.get('url')
        routing_key = data.get('routing_key')
        queue_sender = Common.QueueSender(routing_key)

        threading.Thread(target=asyncio.run, args=(self.checking_thread(queue_sender, service_url),)).start()

        return web.Response(status=200)

    @staticmethod
    def read_dictionary(filename: str) -> list[str]:
        # ToDo найти нормальный словарь
        with open(filename, 'r') as file:
            for url in file.readlines():
                yield url.strip()
