from dictionary_check_server import DictionaryCheckServer

s = DictionaryCheckServer('127.0.0.1', 8082)
s.add_handler('/', 'get', s.check)
s.run()
