from django import forms
from django.core.validators import FileExtensionValidator


class DocumentUploadForm(forms.Form):
    upload_file = forms.FileField(validators=[FileExtensionValidator(allowed_extensions=['yaml'])])


class CheckForm(forms.Form):
    filename = forms.Select()
    random_check = forms.CheckboxInput()
    dictionary_check = forms.CheckboxInput()
    documentation_check = forms.CheckboxInput()
