from django.shortcuts import render, redirect
from django.views import View
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.http.response import JsonResponse
from .models import DocumentationFile, Check
from .forms import DocumentUploadForm, CheckForm
import requests


@method_decorator(login_required, name='dispatch')
class UploadDocumentationView(View):
    def get(self, request, *args, **kwargs):
        customer = request.user.customer
        docs = list(customer.documentationfile_set.all())
        return render(request, 'upload_docs.html', {'docs': docs})

    def post(self, request, *args, **kwargs):
        form = DocumentUploadForm(request.POST, request.FILES)
        if form.is_valid():
            try:
                customer = request.user.customer
                name = request.FILES['upload_file'].name
                doc_file = DocumentationFile(file=request.FILES['upload_file'], owner=customer, name=name)
                doc_file.save()
            except Exception as e:
                return render(request, 'upload_docs.html', {'form': form, 'errors': [e]})
            return JsonResponse({'success': 'url'})
        return render(request, 'upload_docs.html', {'form': form, 'errors': form.errors})


@method_decorator(login_required, name='dispatch')
class CheckDocumentationView(View):
    def post(self, request, *args, **kwargs):
        form = CheckForm(request.POST)
        if form.is_valid():
            try:
                customer = request.user.customer
                if form.data.get('filename') not in [f.file.name for f in customer.documentationfile_set.all()]:
                    return JsonResponse({'error': 'User does not have permissions'})
                checks = []
                if form.data.get('random_check'):
                    checks.append('rand')
                if form.data.get('dictionary_check'):
                    checks.append('dict')
                if form.data.get('documentation_check'):
                    checks.append('doc')
                if not checks:
                    return JsonResponse({'error': 'No checks were selected'})
                doc = customer.documentationfile_set.get(file=form.data.get('filename'))
                file = {'docs': open(doc.file.file.name)}
                check = Check(checks=checks, customer=customer, documentation=doc, status='Started', finished=None)
                check.save()
                requests.post('http://127.0.0.1:8080', files=file, data={'checks': checks, 'id': check.id})
            except Exception as e:
                return render(request, 'upload_docs.html', {'form': form, 'errors': [e]})
            return redirect(to='/')
        return render(request, 'upload_docs.html', {'form': form, 'errors': form.errors})
