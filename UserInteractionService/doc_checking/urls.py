from django.urls import path
from .views import UploadDocumentationView, CheckDocumentationView

urlpatterns = [
    path('upload/', UploadDocumentationView.as_view()),
    path('check/', CheckDocumentationView.as_view()),
]
