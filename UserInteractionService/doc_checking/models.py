from django.db import models
from django.contrib.postgres.fields import ArrayField
from django.core.validators import FileExtensionValidator
from basic_system.models import Customer


class DocumentationFile(models.Model):
    file = models.FileField(validators=[FileExtensionValidator(allowed_extensions=['yaml'])])
    has_been_checked = models.BooleanField(default=False)
    name = models.CharField(max_length=50, blank=False)

    owner = models.ForeignKey(to=Customer, on_delete=models.CASCADE)


class Check(models.Model):
    checks = ArrayField(
        models.CharField(max_length=7),
        blank=False
    )
    started = models.DateTimeField(auto_now_add=True, blank=False)
    finished = models.DateTimeField(blank=True, null=True)
    report_path = models.FilePathField(allow_folders=False, blank=True)
    status = models.CharField(max_length=25, blank=False)

    documentation = models.ForeignKey(to=DocumentationFile, on_delete=models.CASCADE)
    customer = models.ForeignKey(to=Customer, on_delete=models.CASCADE)
