from django.urls import path
from .views import RegisterView, LoginView, HomeView

urlpatterns = [
    path('', HomeView.as_view()),
    path('login/', LoginView.as_view()),
    path('register/', RegisterView.as_view()),
]
