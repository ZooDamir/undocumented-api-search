from django.shortcuts import render, redirect
from django.views import View
from django.core.exceptions import ValidationError
from django.contrib.auth import login, authenticate
from .models import Customer, User
from .forms import RegisterForm, LoginForm
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator


class RegisterView(View):
    def get(self, request, *args, **kwargs):
        form = RegisterForm()
        return render(request, 'register.html', {'form': form})

    def post(self, request, *args, **kwargs):
        form = RegisterForm(request.POST)
        if form.is_valid():
            try:
                password = form.check_password()
            except ValidationError as error:
                return render(request, 'register.html', {'form': form, 'errors': error})
            email = form.cleaned_data['email']
            if Customer.check_email_exists(email):
                return render(request, 'register.html', {'form': form, 'errors': ['email exists']})
            name = form.cleaned_data['email']
            surname = form.cleaned_data['surname']
            user = User.objects.create_user(username=email, email=email, password=password,
                                            first_name=name, last_name=surname)
            customer = Customer(user=user)
            user.save()
            customer.save()
            login(request, user)
        else:
            return render(request, 'register.html', {'form': form})
        return redirect(to='/')


class LoginView(View):
    def get(self, request, *args, **kwargs):
        form = LoginForm()
        return render(request, 'login.html', {'form': form})

    def post(self, request, *args, **kwargs):
        form = LoginForm(request.POST)
        if form.is_valid():
            clean_data = form.cleaned_data
            user = authenticate(request, username=clean_data['login'], password=clean_data['password'])
            if user is not None:
                if user.is_active:
                    login(request, user)
                    return redirect(to='/')
                else:
                    return render(request, 'login.html', {'form': form, 'errors': ['user was deleted!']})
        return render(request, 'login.html', {'form': form, 'errors': form.errors})


@method_decorator(login_required, name='dispatch')
class HomeView(View):
    def get(self, request, *args, **kwargs):
        user = request.user
        checks = list(user.customer.check_set.all())
        files = list(user.customer.documentationfile_set.all())
        return render(request, 'index.html', {'checks': checks, 'files': files})
