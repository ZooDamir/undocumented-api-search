from django.db import models
from django.contrib.auth.models import User


class Customer(models.Model):
    user = models.OneToOneField(to=User, on_delete=models.CASCADE)
    last_check_finished = models.BooleanField(default=True)

    @staticmethod
    def check_email_exists(email):
        output = User.objects.filter(email=email).exists()
        print(output)
        return output
