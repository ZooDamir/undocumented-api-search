function show_register_form() {
    let modal = document.getElementById("registrationModal");
    modal.style.display = "block";

    window.onclick = function(event) {
      if (event.target === modal) {
        modal.style.display = "none";
      }
    }
}