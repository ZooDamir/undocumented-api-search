function show_upload_form() {
    let cookie = getCookie('csrftoken');
    let modal = document.getElementById("uploadModal");

    modal.style.display = "block";
    window.onclick = function(event) {
      if (event.target === modal) {
        modal.style.display = "none";
      }
    }
}