from django.urls import path
from .views import DownloadReportView, ReportReadyView, ReportView

urlpatterns = [
    path('download/', DownloadReportView.as_view()),
    path('<int:id>/', ReportView.as_view()),
    path('<filename>/', ReportReadyView.as_view()),
]
