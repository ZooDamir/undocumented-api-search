import datetime
import json
from django.shortcuts import render
from django.views import View
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
from django.http.response import HttpResponse
from doc_checking.models import Check
import requests


@method_decorator(login_required, name='dispatch')
class DownloadReportView(View):
    def get(self, request, *args, **kwargs):
        user = request.user
        return render(request, 'report.html')

    def post(self, request, *args, **kwargs):
        pass


@method_decorator(csrf_exempt, name='dispatch')
class ReportReadyView(View):
    def post(self, request, *args, **kwargs):
        user = User.objects.get(id=4)
        checks = list(user.customer.check_set.all())
        last = checks[-1]
        last.finished = datetime.datetime.now()
        last.status = 'Finished'
        last.report_path = json.loads(request.body).get('filename')
        last.save()
        return HttpResponse(status=200)


@method_decorator(login_required, name='dispatch')
class ReportView(View):
    def get(self, request, *args, **kwargs):
        customer = request.user.customer
        check_id = kwargs.get('id')
        if check_id not in [x.id for x in customer.check_set.all()]:
            return HttpResponse(403)
        file = requests.get(f'http://127.0.0.1:8084/')
        file = file.content.decode()
        return HttpResponse(file)

    def post(self, request, *args, **kwargs):
        pass
