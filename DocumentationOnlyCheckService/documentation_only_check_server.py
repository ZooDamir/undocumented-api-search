from common_module import Common
import aiohttp
from aiohttp import web
import asyncio
import threading


class DocumentationOnlyCheckServer(Common.CheckingServer):
    @staticmethod
    async def make_all_requests_and_send_on_finish(queue_sender, session: aiohttp.ClientSession, route: Common.Route,
                                                   base_url: str):
        check = await DocumentationOnlyCheckServer.check_route(session, route, base_url)
        if check['failed']:
            print(check)
            queue_sender.send_to_queue({'got': check})

    @staticmethod
    async def check_route(session: aiohttp.ClientSession, route: Common.Route, base_url: str):
        tasks = []
        basic_check = DocumentationOnlyCheckServer.make_basic_check(session, route, base_url)
        tasks.append(basic_check)
        results = await asyncio.gather(*tasks, return_exceptions=True)
        results = DocumentationOnlyCheckServer.collect_check_information(results)
        return results

    @staticmethod
    def collect_check_information(results) -> dict:
        output = {
            'failed': False,
            'failed_checks': []
        }
        for r in results:
            if r['failed']:
                output['failed'] = True
                output['failed_checks'] = {
                    'type': r['check_type'],
                    'url': r['route'],
                    'details': r['details']
                }
        return output

    @staticmethod
    async def make_basic_check(session: aiohttp.ClientSession, route: Common.Route, base_url: str) -> dict:
        output = {
            'check_type': 'basic',
            'failed': False,
            'details': []
        }
        basic_check = await DocumentationOnlyCheckServer.make_all_http_requests(session, base_url + route.url)
        described_methods = [method.name for method in route.methods]
        for result in basic_check['responses']:
            if result['method'].upper() not in described_methods and result['response'].status != 405:
                output['failed'] = True
                output['route'] = route.url
                method = route[result['method'].upper()]
                output['details'].append({
                    'method': result['method'],
                    'expected': method.responses if method else None,
                    'got': result['response'].status,
                    'should_work': method.status == 'active' if method else False
                })
        return output

    @staticmethod
    async def checking_thread(queue_sender, doc: Common.Documentation):
        tasks = []
        async with aiohttp.ClientSession() as session:
            for route in doc.routes:
                tasks.append(DocumentationOnlyCheckServer.make_all_requests_and_send_on_finish(queue_sender, session,
                                                                                               route, doc.host))

            await asyncio.gather(*tasks, return_exceptions=True)

        queue_sender.send_to_queue({'status': 'finished', 'service': 'doc'})

    @staticmethod
    async def check(request: aiohttp.web.Request):
        data = await request.json()
        doc = Common.Documentation.from_json(data.get('doc'))
        routing_key = data.get('routing_key')
        queue_sender = Common.QueueSender(routing_key)

        threading.Thread(target=asyncio.run,
                         args=(DocumentationOnlyCheckServer.checking_thread(queue_sender, doc), )).start()

        return web.Response(status=200)
