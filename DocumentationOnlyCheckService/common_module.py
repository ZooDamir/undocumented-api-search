import pathlib
import sys
common_module_path = pathlib.Path().absolute().parent
sys.path.insert(0, str(common_module_path))
import Common as _Common

Common = _Common
