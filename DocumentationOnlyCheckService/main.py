from documentation_only_check_server import DocumentationOnlyCheckServer


s = DocumentationOnlyCheckServer('127.0.0.1', 8081)
s.add_handler('/', 'get', DocumentationOnlyCheckServer.check)
s.run()
