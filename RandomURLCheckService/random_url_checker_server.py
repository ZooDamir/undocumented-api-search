import asyncio
import aiohttp
from aiohttp import web
import random
from common_module import Common
import threading


class RandomURLCheckerServer(Common.CheckingServer):
    @staticmethod
    async def generate_random_string(length: int) -> str:
        return ''.join(random.choice('0123456789abcdef') for _ in range(length))

    @staticmethod
    async def generate_random_strings(amount: int):
        sizes = [32, 40, 56, 64, 96, 128]
        for i in range(amount):
            for size in sizes:
                yield await RandomURLCheckerServer.generate_random_string(size)

    @staticmethod
    async def make_all_requests_and_send_on_finish(queue_sender, session: aiohttp.ClientSession, url: str):
        response = await RandomURLCheckerServer.make_all_http_requests(session, url)
        response = {'url': response['url'],
                    'methods_available': [x['method'] for x in response['responses'] if x['response'].status != 404]}
        if response['methods_available']:
            queue_sender.send_to_queue({'got': response})
        else:
            print(f'[NO METHODS AVAILABLE] FOR {url}')

    @staticmethod
    async def checking_thread(queue_sender, service_url: str):
        tasks = []
        async with aiohttp.ClientSession() as session:
            async for s in RandomURLCheckerServer.generate_random_strings(100):
                tasks.append(
                    RandomURLCheckerServer.make_all_requests_and_send_on_finish(queue_sender,
                                                                                session, service_url + f'/{s}'))

            await asyncio.gather(*tasks, return_exceptions=True)

        queue_sender.send_to_queue({'status': 'finished', 'service': 'rand'})

    async def check(self, request: aiohttp.web.Request):
        data = await request.json()
        service_url = data.get('url')
        routing_key = data.get('routing_key')
        queue_sender = Common.QueueSender(routing_key)

        threading.Thread(target=asyncio.run, args=(self.checking_thread(queue_sender, service_url), )).start()

        return web.Response(status=200)
