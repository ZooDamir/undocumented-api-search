from random_url_checker_server import RandomURLCheckerServer

s = RandomURLCheckerServer('127.0.0.1', 8083)
s.add_handler('/', 'get', s.check)
s.run()
