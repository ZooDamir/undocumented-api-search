import abc
import asyncio
from aiohttp import web
import aiohttp
from .queue_sender import QueueSender
import typing


class Server(abc.ABC):
    def __init__(self, host: str, port: int):
        self._host_ = host
        self._port_ = port
        self._routes_ = []

    @abc.abstractmethod
    def run(self):
        pass

    def add_handler(self, route: str, method: str, handler):
        method = method.lower()
        if method == 'get':
            self._routes_.append(web.get(route, handler))
        elif method == 'post':
            self._routes_.append(web.post(route, handler))
        elif method == 'put':
            self._routes_.append(web.put(route, handler))
        elif method == 'delete':
            self._routes_.append(web.delete(route, handler))
        elif method == 'head':
            self._routes_.append(web.head(route, handler))
        else:
            raise Exception(f"HTTP METHOD {method} DOESN'T EXISTS")


class CheckingServer(Server):
    def __init__(self, host: str, port: int):
        super(CheckingServer, self).__init__(host, port)
        self._app_ = web.Application()

    def run(self):
        self._app_.add_routes(self._routes_)
        web.run_app(self._app_, host=self._host_, port=self._port_)

    @staticmethod
    async def get_request(session: aiohttp.ClientSession, url: str):
        async with session.get(url) as response:
            return {
                'method': 'get',
                'response': response
            }

    @staticmethod
    async def post_request(session: aiohttp.ClientSession, url: str):
        async with session.post(url) as response:
            return {
                'method': 'post',
                'response': response
            }

    @staticmethod
    async def put_request(session: aiohttp.ClientSession, url: str):
        async with session.put(url) as response:
            return {
                'method': 'put',
                'response': response
            }

    @staticmethod
    async def delete_request(session: aiohttp.ClientSession, url: str):
        async with session.delete(url) as response:
            return {
                'method': 'delete',
                'response': response
            }

    @staticmethod
    @abc.abstractmethod
    async def check(request: aiohttp.web.Request):
        pass

    @staticmethod
    async def make_all_http_requests(session: aiohttp.ClientSession, url: str):
        tasks = [
            CheckingServer.get_request(session, url),
            CheckingServer.post_request(session, url),
            CheckingServer.put_request(session, url),
            CheckingServer.delete_request(session, url),
        ]
        responses = await asyncio.gather(*tasks, return_exceptions=True)
        output = {
            'url': url,
            'responses': responses
        }

        return output

    @staticmethod
    @abc.abstractmethod
    async def make_all_requests_and_send_on_finish(queue_sender: QueueSender, session: aiohttp.ClientSession, url: str):
        pass
