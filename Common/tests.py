import unittest
import yaml
from models import Documentation
import asyncio
from queue_sender import QueueSender


class TestDocumentationFormation(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_json_conversion(self):
        with open('Swagger.yaml') as file:
            data = yaml.safe_load(file)
            doc = asyncio.get_event_loop().run_until_complete(Documentation.create_from_file(data))
            t = doc.to_json()
            doc = Documentation.from_json(t)
            t1 = doc.to_json()
            self.assertEqual(t, t1)


if __name__ == "__main__":
    unittest.main()
