import asyncio
import abc
import json
import yaml
from dataclasses import dataclass
from typing import Optional


class JSONSerializable(abc.ABC):
    def to_json(self, indent: Optional[int] = None):
        raise NotImplementedError

    @staticmethod
    @abc.abstractmethod
    def from_json(json_str: str):
        raise NotImplementedError


class Schema(JSONSerializable):
    """
    Класс для представления какого-либо ресурса (схемы)
    """
    @dataclass
    class Property:
        """
        Класс для представления данных о каком-либо свойстве (поле) ресурса
        """
        name: str  # название свойства
        type: str  # тип данных ресурса
        is_required: bool  # обязательно ли данное поле для этого ресурса

        def __repr__(self):
            """
            Метод для форматированного вывода информации об объекте
            :return: строковое представление объекта
            """
            temp = ''
            if self.is_required:
                temp = '*'
            return f'{self.type}{temp}'

    class SchemaEncoder(json.JSONEncoder):
        def default(self, obj):
            if isinstance(obj, Schema):
                temp = dict()
                temp['name'] = obj.name
                temp['fields'] = dict()
                for field in obj.fields:
                    temp['fields'][field.name] = ({'type': field.type, 'is_required': field.is_required})
                return temp
            raise TypeError(f'Expected Schema type, got {type(obj)}')

    def __init__(self, name: str, properties: dict, required: list):
        """
        Конструктор класса
        По данным из документации создаёт объект класса Schema
        :param name: Название данного ресурса
        :param properties: Свойства (поля) этого ресурса
        :param required: Список обязательных полей
        """
        self.name = name  # Задаём имя ресурса
        self.fields = []

        for prop in properties:  # проходимся по всем переданным полям
            if '$ref' in properties[prop]:  # если поле является ссылкой на какой-либо другой ресурс
                last_slash = properties[prop]['$ref'].rfind('/') + 1  # Получаем индекс последнего слуша
                property_type = properties[prop]['$ref'][last_slash:]  # Формируем имя
                # Делаем мы это таким образом т.к. ссылки в документации имеют вид: #/definitions/<название типа>
            elif 'items' in properties[prop]:
                property_type = properties[prop]['items']  # Иначе тип - примитив, поэтому заносим просто так
            else:
                property_type = properties[prop]['type']
            self.fields.append(Schema.Property(prop, property_type, is_required=prop in required))  # Заносим в список полей

    def __repr__(self):
        """
        Метод для форматированного вывода информации об объекте
        :return: строковое представление объекта
        """
        return f'{self.name.capitalize()}: {self.fields}'

    def to_json(self, indent=None) -> str:
        """
        Метод для формирования JSON'а из объекта
        :param indent: форматирование с помощью отступов
        :return: строка - JSON закодированный объект
        """
        return json.dumps(self, indent=indent, cls=Schema.SchemaEncoder)

    @staticmethod
    def from_json(json_str):
        """
        Метод формирования объекта из JSON строки
        :param json_str: JSON-закодированный объект
        :return: объект класса Schema
        """
        data = json.loads(json_str)  # получаем словарь из сторки
        temp = Schema(data['name'], dict(), [])  # формируем временный объект схемы с пустыми полями
        for field in data['fields']:  # заносим информацию о полях
            temp.fields.append(Schema.Property(field, data['fields'][field]['type'], data['fields'][field]['is_required']))
        return temp


class Route(JSONSerializable):
    class Method:
        class Parameter:
            """
            Класс для представления параметра в конкретном запросе (методе)
            """

            def __init__(self, parameter_description: dict):
                self.name = parameter_description.get('name') or None  # записываем имя
                self.send_in = parameter_description.get('in') or None  # где передаётся (тело/url/заголовки?)
                self.is_required = parameter_description.get('required') or None  # обязателен ли
                if 'schema' in parameter_description:
                    if 'type' not in parameter_description['schema']:
                        last_slash = parameter_description['schema']['$ref'].rfind('/') + 1
                        schema_name = parameter_description['schema']['$ref'][last_slash:]
                        self.schema = {
                            'type': 'single',
                            'name': schema_name
                        }
                    else:
                        last_slash = parameter_description['schema']['items']['$ref'].rfind('/') + 1
                        schema_name = parameter_description['schema']['items']['$ref'][last_slash:]
                        self.schema = {
                            'type': 'multiple',
                            'name': 'array',
                            'items': {
                                'type': schema_name,
                            }
                        }
                elif 'type' in parameter_description:
                    self.schema = {
                        'type': 'single',
                        'name': parameter_description['type']
                    }
                    if parameter_description['type'] == 'array':
                        self.schema['items'] = parameter_description['items']
                        self.schema['type'] = 'multiple'

            def __repr__(self):
                temp = ''
                if self.is_required:
                    temp = '*'
                temp2 = ''
                temp3 = ''
                temp4 = self.schema["name"]
                if self.schema['type'] == 'multiple':
                    temp2 = '['
                    temp3 = ']'
                    temp4 = self.schema['items']['type']

                return f'{self.name}{temp}:{temp2}{temp4}{temp3} sends in {self.send_in}'

            @staticmethod
            def from_dict(description: dict):
                parameter = Route.Method.Parameter({})
                parameter.name = description['name']
                parameter.is_required = description['is_required']
                parameter.send_in = description['send_in']
                parameter.schema = description['schema']
                return parameter

        def __init__(self, name: str, description: dict, status: str):
            self.name = name.upper()  # название метода (в терминах HTTP методов)
            self.parameters = []  # список параметров для данного метода
            self.status = status  # статус данного метода для соответсвующего URL
            self.input_serialization = description.get('consumes') or None  # в каком виде передавать параметры (JSON/XML)
            self.returns = description.get('produces') or None  # в каком виде приходит ответ (JSON/XML/plaintext)
            self.responses = description.get('responses') or {}  # ожидаемые коды ответов
            self.responses = [int(x) if x != 'default' else 200 for x in self.responses.keys()]

            for parameter in description.get('parameters') or []:  # формируем список параметров
                self.parameters.append(self.Parameter(parameter))

        def __repr__(self):
            """
            Метод для форматированного вывода информации об объекте
            :return: строковое представление объекта
            """
            status = ''
            if self.status == 'deprecated':
                status = '[DEPRECATED] '
            return f'{status}{self.name.capitalize()}: {self.parameters}'

        @staticmethod
        def from_dict(description: dict):
            temp = Route.Method(description['name'], {}, description['status'])
            temp.input_serialization = description['input_serialization']
            temp.returns = description['returns']
            temp.responses = description['responses']
            for parameter in description['parameters']:
                temp.parameters.append(Route.Method.Parameter.from_dict(parameter))
            return temp

    class RouteEncoder(json.JSONEncoder):
        def default(self, obj):
            if isinstance(obj, Route):
                temp = dict()
                temp['url'] = obj.url
                temp['methods'] = list()
                for method in obj.methods:
                    params = list()
                    for parameter in method.parameters:
                        params.append({
                            'name': parameter.name,
                            'is_required': parameter.is_required,
                            'send_in': parameter.send_in,
                            'schema': parameter.schema
                        })

                    temp['methods'].append(
                        {'name': method.name,
                         'status': method.status,
                         'parameters': params,
                         'returns': method.returns,
                         'input_serialization': method.input_serialization,
                         'responses': method.responses
                         }
                    )
                return temp
            raise TypeError(f'Expected Route type, got {type(obj)}')

    def __init__(self, url: str, definitions: dict):
        self.url = url  # записываем данный url
        self.methods = []  # формируем пустой список методов, которые он принимает

        for method in definitions:  # проходимся по всем методам в документации
            status = 'deprecated' if definitions[method].get('deprecated') else 'active'  # выясняем статус
            self.methods.append(Route.Method(method, definitions[method], status))  # заносим их в виде объектов

    def __repr__(self):
        """
        Метод для форматированного вывода информации об объекте
        :return: строковое представление объекта
        """
        temp = '\n\t'.join([repr(x) for x in self.methods])
        return f'{self.url}:\n\t{temp}'

    def __getitem__(self, item: str):
        described = [m.name.upper() for m in self.methods]
        if item.upper() not in described:
            return None
        else:
            for m in self.methods:
                if m.name == item.upper():
                    return m

    @staticmethod
    def from_json(json_str: str):
        temp = Route('', {})
        deserialized = json.loads(json_str)
        temp.url = deserialized.get('url')
        for method in deserialized.get('methods'):
            temp.methods.append(Route.Method.from_dict(method))
        return temp

    def to_json(self, indent: Optional[int] = None):
        return json.dumps(self, indent=indent, cls=Route.RouteEncoder)


@dataclass
class Documentation(JSONSerializable):
    host: str
    schemas: list[Schema]
    routes: list[Route]

    def __repr__(self):
        output = ''
        for schema in self.schemas:
            output += repr(schema)
            output += '\n'
        output += '-' * 100
        output += '\n'
        for route in self.routes:
            output += repr(route)
            output += '\n'
        return output

    @staticmethod
    def from_json(json_str: str):
        deserialized = json.loads(json_str)
        temp = Documentation(deserialized['host'], [], [])
        for schema in deserialized['schemas']:
            temp.schemas.append(Schema.from_json(schema))
        for route in deserialized['routes']:
            temp.routes.append(Route.from_json(route))
        return temp

    def to_json(self, indent: Optional[int] = None):
        temp = dict()
        temp['host'] = self.host
        temp['schemas'] = [Schema.to_json(x) for x in self.schemas]
        temp['routes'] = [Route.to_json(x) for x in self.routes]
        return json.dumps(temp, indent=indent)

    @staticmethod
    async def _create_schema_list_from_definitions_(definitions: dict) -> list[Schema]:
        """
        Функция для формирования списка описаний ресурсов
        :param definitions: словарь из YAML файла с описанием ресурсов данного API
        :return: список ресурсов
        """
        output = []  # подготавливаем выходной массив
        for definition in definitions:  # проходимся по всем ресурсам (схемам)
            req = []  # подготавливаем список обязательных полей для очередного ресурса
            if 'required' in definitions[definition]:  # если он (список) есть у ресурса
                req = definitions[definition]['required']  # то заносим в список те поля, которые там есть
            output.append(
                Schema(definition, definitions[definition]['properties'], req))  # в output заносим новый объект схемы
        return output

    @staticmethod
    async def _create_route_list_from_definitions_(definitions: dict) -> list[Route]:
        """
        Функция для формирования списка роутов
        :param definitions: словарь из YAML файла с описанием роутов данного API
        :return: список роутов (подготовленных URL путей)
        """
        output = []
        for route in definitions:
            output.append(Route(route, definitions[route]))
        return output

    @staticmethod
    async def create_from_file(parsed_doc):
        schemas, routes = await asyncio.gather(
            asyncio.create_task(Documentation._create_schema_list_from_definitions_(parsed_doc.get('definitions') or [])),
            asyncio.create_task(Documentation._create_route_list_from_definitions_(parsed_doc['paths']))
        )

        return Documentation(parsed_doc['host'], schemas, routes)
