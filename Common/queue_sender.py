import json
import pika


class QueueSender:
    def __init__(self, routing_key, exchange='Diplom', queue_host='localhost'):
        self._exchange_ = exchange
        self._connection_ = pika.BlockingConnection(pika.ConnectionParameters(host=queue_host))
        self._channel_ = self._connection_.channel()
        self._routing_key_ = routing_key
        self._channel_.exchange_declare(exchange=self._exchange_, exchange_type='topic')

    def send_to_queue(self, data: dict):
        data = json.dumps(data).encode()
        self._channel_.basic_publish(exchange=self._exchange_, routing_key=self._routing_key_, body=data)
