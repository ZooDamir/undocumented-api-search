from flask import Flask, request
from hashlib import sha1

app = Flask(__name__)


@app.route('/', methods=['GET', 'POST'])
def hello_world():
    return 'Hello World!'


@app.route('/queryArg')
def query_arg():
    name = request.args.get('name')
    if name:
        return name
    else:
        return 'Error'


@app.route(f'/{sha1("Some".encode()).hexdigest()}')
def naive_secure():
    return 'Some sensitive data'


@app.route('/admin')
def admin():
    return 'Admin page!'


if __name__ == '__main__':
    app.run()
