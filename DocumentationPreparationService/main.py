from documentation_preparation_server import DocumentationPreparationServer

s = DocumentationPreparationServer('127.0.0.1', 8080)
s.add_handler('/', 'post', s.check_doc_and_prepare)
s.run()
# print(asyncio.get_event_loop().run_until_complete(check_site_availability('https://google.com')))
