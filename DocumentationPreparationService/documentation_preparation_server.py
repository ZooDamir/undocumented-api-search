import time
import yaml
from common_module import Common
import asyncio
import aiohttp
from aiohttp import web
import re
import json
import threading


class FileCheckError(Exception):
    pass


class DocumentationPreparationServer(Common.Server):
    def __init__(self, host: str, port: int):
        super(DocumentationPreparationServer, self).__init__(host, port)
        self._settings_ = self.load_settings('settings.json')
        self._app_ = web.Application()
        self._check_requests_ = {
            'doc': self._request_only_doc_check_,
            'dict': self._request_dictionary_check_,
            'rand': self._request_random_check_
        }

    @staticmethod
    def load_settings(settings_filename: str) -> dict:
        with open(settings_filename) as file:
            settings = json.loads(file.read())
            return settings

    @staticmethod
    async def _check_site_availability_(site_url: str) -> bool:
        """
        Делает асинхронный запрос к сайту и возвращает был ли статус ответа 200
        :param site_url: URL сайта, который нужно проверить
        :return: True, если сайт доступен, False в ином случае
        """
        try:
            async with aiohttp.ClientSession() as session:
                async with session.get(site_url) as response:
                    return True
        except Exception:
            return False

    @staticmethod
    def _check_file_content_(file) -> dict:
        try:
            data = yaml.safe_load(file)
            if 'host' not in data:
                raise FileCheckError('Host not found')
            if 'paths' not in data:
                FileCheckError('Routes needed')
        except Exception as e:
            raise FileCheckError(e)

        return data

    @staticmethod
    def _check_host_(parsed_file: dict) -> str:
        host: str = parsed_file['host']
        schemes = parsed_file.get('schemes') or 'https'
        appropriate_url = re.compile(r'http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+')
        if re.fullmatch(appropriate_url, host):
            return host
        else:
            if type(schemes) == list:
                host = schemes[0] + '://' + host
                if re.fullmatch(appropriate_url, host):
                    return host
            else:
                host = schemes + '://' + host
                if re.fullmatch(appropriate_url, host):
                    return host

        raise FileCheckError('bad host url')

    async def check_doc_and_prepare(self, request: aiohttp.web.Request):
        data = await request.post()
        documentation = data.get('docs')
        if 'checks' in data:
            checks = data.getall('checks')
        else:
            checks = []
        if not checks or not documentation:
            return web.json_response({'error': 'No docs or checks!'})  # ToDo add status to response
        try:
            file_to_parse = documentation.file.file
            parsed_file = DocumentationPreparationServer._check_file_content_(file_to_parse)
            real_host = DocumentationPreparationServer._check_host_(parsed_file)
            available = await DocumentationPreparationServer._check_site_availability_(real_host)
            if not available:
                raise FileCheckError('site not available')
        except FileCheckError as e:
            return web.json_response({'error': 'Some error!'})  # ToDo add status to response

        doc = await Common.Documentation.create_from_file(parsed_file)
        doc.host = real_host

        check_id = data.get('id')
        routing_key = await self._get_queue_routing_key_(doc, checks, check_id)

        tasks = []
        for check in checks:
            tasks.append(self._check_requests_[check](url=real_host, doc=doc, routing_key=routing_key))

        threading.Thread(target=asyncio.run, args=(self.start_checking(tasks), )).start()

        return web.Response(status=200)

    async def _get_queue_routing_key_(self, documentation: Common.Documentation, checks: list[str], check_id) -> str:
        rcs_host = self._settings_.get('report_creation_service')
        data = {
            'documentation': documentation.to_json(),
            'checks': checks,
            'check_id': check_id
        }
        while True:
            try:
                async with aiohttp.ClientSession() as session:
                    async with session.post(rcs_host + '/start_queue', json=data) as response:
                        if response.status == 200:
                            data = await response.json()
                            return data.get('routing_key')
            except Exception as e:
                print(f'[GOT ERROR WHILE GETTING QUEUE NAME] {e}')

    async def start_checking(self, tasks):
        await asyncio.gather(*tasks, return_exceptions=True)

    @staticmethod
    async def _make_request_to_check_(service, json_param: dict):
        while True:
            try:
                async with aiohttp.ClientSession() as session:
                    async with session.get(service, json=json_param) as response:
                        if response.status == 200:
                            break
            except Exception as e:
                print(f'[GOT ERROR IN CHECK SERVICE] {e}')
            time.sleep(1)

    async def _request_only_doc_check_(self, **kwargs):
        doc = kwargs.get('doc')
        routing_key = kwargs.get('routing_key')
        doc = doc.to_json()
        data = {
            'doc': doc,
            'routing_key': routing_key
        }
        await self._make_request_to_check_(self._settings_.get('doc_check_service'), data)

    async def _request_dictionary_check_(self, **kwargs):
        url = kwargs.get('url')
        routing_key = kwargs.get('routing_key')
        data = {
            'url': url,
            'routing_key': routing_key
        }
        await self._make_request_to_check_(self._settings_.get('dictionary_check_service'), data)

    async def _request_random_check_(self, **kwargs):
        url = kwargs.get('url')
        routing_key = kwargs.get('routing_key')
        data = {
            'url': url,
            'routing_key': routing_key
        }
        await self._make_request_to_check_(self._settings_.get('random_check_service'), data)

    def run(self):
        self._app_.add_routes(self._routes_)
        web.run_app(self._app_, host=self._host_, port=self._port_)
