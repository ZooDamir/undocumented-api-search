import time
import requests
from common_module import Common
from queue_reader import QueueReader
import hashlib
import datetime
from aiohttp import web
from jinja2 import Environment, FileSystemLoader
import aiohttp
import threading


class ReportCreationServer(Common.Server):
    def __init__(self, host: str, port: int, templates_folder: str):
        super(ReportCreationServer, self).__init__(host, port)
        self._app_ = web.Application()
        template_loader = FileSystemLoader(searchpath=templates_folder)
        self._env_ = Environment(loader=template_loader)

    def render_template(self, template_name: str, details: dict):
        template = self._env_.get_template(template_name)
        return template.render(details)

    def process_errors(self, errors: dict, documentation: Common.Documentation):
        output = []
        for error in errors:
            if error:
                if 'methods_available' in error:
                    for method in error['methods_available']:
                        for route in documentation.routes:
                            if route.url == '/'.join(error['url'].split('/')[3:]):
                                for m in route.methods:
                                    if 200 not in m.responses:
                                        output.append({
                                            'url': method + ' ' + error['url'],
                                            'expected': m.responses,
                                            'got': 200
                                        })
                        else:
                            output.append({
                                'url': method + ' ' + error['url'],
                                'expected': None,
                                'got': 200
                            })
                elif 'failed_checks' in error:
                    for detail in error['failed_checks']['details']:
                        output.append({
                            'url': detail['method'] + ' ' + error['failed_checks']['url'],
                            'expected': detail['expected'],
                            'got': detail['got']
                        })

        return output

    def form_report(self, details: dict, checks, documentation: Common.Documentation, check_id):
        clean_details = self.process_errors(details['errors'], documentation)
        html = self.render_template('index.html', {'errors': clean_details,
                                                   'url': documentation.host})
        filename = 'smth.html'
        with open(filename, 'w') as file:
            file.write(html)
            requests.post(f'http://127.0.0.1:8000/reports/{filename}/', json={
                'filename': filename,
                'checks': checks,
                'id': check_id
            })

    def run(self):
        self._app_.add_routes(self._routes_)
        web.run_app(self._app_, host=self._host_, port=self._port_)

    def create_report_on_finish(self, qr: QueueReader, documentation: Common.Documentation, checks):
        while True:
            if not qr.running():
                self.form_report(qr.errors(), checks, documentation, qr.check_id)
                return
            time.sleep(1)

    async def start_queue(self, request: aiohttp.web.Request):
        data = await request.json()
        documentation = data.get('documentation')
        checks = data.get('checks')
        check_id = data.get('check_id')
        rk_gen_base = datetime.datetime.now().timestamp().hex() + documentation
        documentation = Common.Documentation.from_json(documentation)
        routing_key = hashlib.md5(rk_gen_base.encode()).hexdigest()
        qr = QueueReader(routing_key, checks, check_id=check_id)
        threading.Thread(target=qr.run).start()
        threading.Thread(target=self.create_report_on_finish, args=(qr, documentation, checks)).start()
        return web.json_response({'routing_key': routing_key})

    async def report_file(self, request: aiohttp.web.Request):
        return aiohttp.web.FileResponse('smth.html', status=200)
