import pika
import json


class QueueReader:
    def on_message(self, channel, method_frame, header_frame, body):
        data = json.loads(body)
        if data.get('status') == 'finished':
            self._checks_.remove(data.get('service'))

        if not self._checks_:
            self._channel_.close()
            self._running_ = False
            return

        self._errors_description_['errors'].append(data.get('got'))

    def __init__(self, target_routing_key: str, checks: list[str], exchange='Diplom', queue_host='localhost', **kwargs):
        self._connection_ = pika.BlockingConnection(pika.ConnectionParameters(host=queue_host))
        self._channel_ = self._connection_.channel()
        self._channel_.exchange_declare(exchange=exchange, exchange_type='topic')
        self._target_routing_key_ = target_routing_key
        result = self._channel_.queue_declare('', exclusive=True)
        self._queue_name_ = result.method.queue
        self._channel_.queue_bind(exchange=exchange, queue=self._queue_name_, routing_key=self._target_routing_key_)
        self._errors_description_ = {'errors': [], 'url': 'Some Server'}
        self._running_ = False
        self._checks_ = checks
        self.check_id = kwargs.get('check_id')

    def running(self):
        return self._running_

    def errors(self):
        return self._errors_description_

    def run(self):
        print(f'[{self._queue_name_}] Waiting for messages.')
        self._running_ = True
        self._channel_.basic_consume(queue=self._queue_name_, on_message_callback=self.on_message, auto_ack=True)
        self._channel_.start_consuming()
