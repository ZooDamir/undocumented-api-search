from report_creation_server import ReportCreationServer


s = ReportCreationServer('127.0.0.1', 8084, 'templates')
s.add_handler('/start_queue', 'post', s.start_queue)
s.add_handler('/', 'get', s.report_file)
s.run()
